export default {
    'N°': 'id',
    'Icone': 'icon',
    'Titre': 'title',
    'Ville': 'city',
    'Jour': 'day',
    'Online': 'isOnline',
    'Format': 'type',
    'Ouverture': 'openAt',
    'Fermeture': 'closeAt',
    'Début du tournoi': 'startAt',
    'Fin du tournoi': 'endAt',
    'Adresse': 'address',
    'Génération auto': 'creationAuto',
    'Lien d\'inscription personnalisé': 'customLink',
    'Lien raccourcis': 'shortLink',
    'Freeplay': 'freeplay',
    'Activé': 'isActive',
    'Setups': 'setups',
    'Joueurs': 'maxPlayers',
    'Nourriture': 'hasFood',
    'Stream': 'stream',
    'Condition d\'entrée': 'price',
    'Informations': 'info',
    'ID du canal': 'channelId'
};