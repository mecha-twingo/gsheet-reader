import express from 'express';
import axios from 'axios';
import convertedFields from './convertedFields.js'

const app = express();

app.get('/:gsheetId', async (req, res) => {

  const convertValues = (value) => {
    if (value === 'TRUE') return true;
    if (value === 'FALSE') return false;
    return value;
  }

  const eventFormat = (wklies) => wklies.reduce((acc, wkly) => [
    ...acc,
    Object.entries(wkly).reduce((obj, [key, value]) => ({ ...obj, [convertedFields[key]]: convertValues(value) }), {})
  ], []);

  try {
    const { data: events } = await axios.get(`https://opensheet.elk.sh/${req.params.gsheetId}/1`)
    res.json(eventFormat(events))
  } catch ({ response }) {
    res.status(response.status).json(response.data);
  }
});

app.listen(8000, () => console.log('Server successfully started!'));

