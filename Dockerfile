FROM node:19
ARG NODE_ARG="development"
ENV NODE_ENV=$NODE_ARG

WORKDIR /app

COPY ./src /app/src
COPY ./entrypoint.sh /app/
COPY ./package*.json /app/

RUN if [ "$NODE_ENV" = "production" ]; then npm ci --omit=dev; fi

ENTRYPOINT ./entrypoint.sh