#!/usr/bin/env bash

set -e

if ([ ! -e node_modules ] && [ ! "$NODE_ENV" = "production" ]) 
then
  npm i
fi

case $NODE_ENV in
  "development")
    ./node_modules/.bin/nodemon src/index.js
    ;;

  "production")
  node src/index.js
    ;;
esac
